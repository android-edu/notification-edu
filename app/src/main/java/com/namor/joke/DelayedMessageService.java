package com.namor.joke;

import android.app.IntentService;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class DelayedMessageService extends IntentService {

    public static final String EXTRA_MESSAGE = "message";
    public static final int NOTIFICATION_ID = 123;

    public DelayedMessageService() {
        super("DelayedMessageService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        synchronized (this) {
            try {
                wait(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        String message = intent.getStringExtra(EXTRA_MESSAGE);
        showText(message);
    }

    private void showText(String message) {
        Log.v("DelayedMessageService", message);
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this, "notify_001")
                        .setSmallIcon(android.R.drawable.sym_def_app_icon)
                        .setContentTitle(getResources().getString(R.string.question))
                        .setContentText(message)
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .setVibrate(new long[] {0, 1000})
                        .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                        .setAutoCancel(true);

        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(
                this,
                0,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT
        );

        builder.setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        assert notificationManager != null;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    "notify_001",
                    "Joke channel",
                    NotificationManager.IMPORTANCE_HIGH
            );
            notificationManager.createNotificationChannel(channel);
            builder.setChannelId("notify_001");
        }

        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }
}
